package controllers

import (
  "github.com/revel/revel"
  "myapps/webapp/app/routes"
)


type Calc struct {
  *revel.Controller
}

func (c Calc) Index(total int,valor int) revel.Result {
  title := "calculadora"
	return c.Render(title,total,valor)
}
func (c Calc) Somar(total int,valor int) revel.Result {
  total = c.Soma(total,valor)
  return c.Redirect(routes.Calc.Index(total,valor))
}
func (c Calc) Soma(total int,valor int) int {
  return total + valor;
}
