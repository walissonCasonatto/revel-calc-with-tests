package tests

import (
  "github.com/revel/revel/testing"
  "myapps/webapp/app/controllers"
)


type CalcTest struct {
	testing.TestSuite
  Calc        controllers.Calc
}

func (t *CalcTest) Before() {
	println("Set up")
}

func (t *CalcTest) TestCalcPageWork() {
	t.Get("/calc/2/3")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
}


func (t *CalcTest) TestaSoma(){
  expected := 10
  current := t.Calc.Soma(3,7)
  t.Assert(expected == current)
}

func (t *CalcTest) After() {
	println("Tear down")
}
